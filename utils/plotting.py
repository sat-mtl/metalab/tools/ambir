#!/usr/bin/env python3
import matplotlib.pyplot as plt
from matplotlib import cm, colors
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import mpl_toolkits.mplot3d as plt3d


def spherical_plot(data: np.ndarray, theta, phi, mode=1, title='title'):
    # Mode 1 corresponds to a colorplot over the sphere. For mode 3, r is
    # a function of theta and phi related to the data
    if mode == 1:
        r = 1
    elif mode == 2:
        r = data
    x, y, z = spherical_to_cartesian(r, theta, phi)
    norm = colors.Normalize(np.min(data), np.max(data))
    fig, ax = plt.subplots(subplot_kw=dict(projection='3d'), figsize=(14, 10))
    ax.plot_surface(
        x,
        y,
        z,
        rstride=1,
        cstride=1,
        facecolors=cm.jet(
            norm(data)))
    lim = np.max(r)
    ax.set_xlim(-lim, lim)
    ax.set_ylim(-lim, lim)
    ax.set_zlim(-lim, lim)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    ax.set_title(title, fontsize=20)
    m = cm.ScalarMappable(cmap=cm.jet)
    m.set_array(data)
    fig.colorbar(m, shrink=0.8)
    plt.show()


def spherical_to_cartesian(r, theta, phi):
    r = np.abs(r)
    x = r * np.cos(phi) * np.cos(theta)
    y = r * np.cos(phi) * np.sin(theta)
    z = r * np.sin(phi)

    return x, y, z

# Project a sphere on the plane


def projection_2d(theta, phi, projection='squircle'):
    theta -= np.pi
    a = np.max(theta)
    b = np.max(phi)
    theta *= 1 / a
    phi *= 1 / b
    if projection == 'aitoff':
        theta = (2 * np.sqrt(2) * np.cos(phi) * np.sin(theta / 2)) / \
            (np.sqrt(1 + np.cos(phi) * np.cos(theta / 2)))
        phi = (np.sqrt(2) * np.sin(phi)) / \
            (np.sqrt(1 + np.cos(phi) * np.cos(theta / 2)))
    elif projection == 'squircle':
        theta = theta * np.sqrt(1 - phi**2 / 2)
        phi = phi * np.sqrt(1 - theta**2 / 2)
    theta *= a
    phi *= b
    theta += np.pi
    return theta, phi

# Manual legend


def projection_legend(ax):

    a = 14
    ax.text(np.pi - 0.5, -np.pi / 2 - 0.4, 'azimuth (deg)', fontsize=a)
    ax.text(0 + 0.8, -np.pi / 2, '0°', fontsize=a)
    ax.text(np.pi, -np.pi / 2 - 0.2, '180°', fontsize=a)
    ax.text(np.pi * 2 - 0.8, -np.pi / 2, '360°', fontsize=a)

    ax.text(0 - 0.7, 0 - 0.5, 'longitude (deg)', rotation=90, fontsize=a)
    ax.text(0, -np.pi / 2 + 0.2, '-180°', fontsize=a)
    ax.text(0 - 0.4, 0, '0°', fontsize=a)
    ax.text(0 + 0.3, np.pi / 2 - 0.2, '180°', fontsize=a)
