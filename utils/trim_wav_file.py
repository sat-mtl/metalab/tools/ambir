#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import soundfile as sf
import signal_processing as U

parser = argparse.ArgumentParser(
    description='Trim .wav file')
parser.add_argument(
    '-f', '--file_name',
    required=True,
    metavar='',
    help='file to trim')

args = parser.parse_args()


def trim_wav_file(file_name):

    signal, fs = sf.read(file_name)
    signal = U.select_chunk(signal)
    sf.write('trimmed' + file_name, signal, fs)


if __name__ == "__main__":
    trim_wav_file(args.file_name)
