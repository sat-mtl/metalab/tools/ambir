s.options.numOutputBusChannels = 60;
s.boot;
s.meter;

//Read CSV and generate delay and amplitude adjustment arrays
(
var data = TabFileReader.read(
    path: thisProcess.nowExecutingPath.dirname +/+ "MAXCSV_goupillon_test1.csv",
    skipEmptyLines: true
);
data.removeAt(0);

~delays = data.flop[2];
~delays = ~delays.asFloat;
~delays = ~delays.maxItem - ~delays;

~amps = data.flop[4];
~amps = ~amps.asFloat;
~amps = ~amps.minItem / ~amps;
)


// Impulse (volume and time between impulses should sound constant at sweetspot)
(
SynthDef(\impulse_spread, { |out=0, amp=0.1, decay=0.015|
    var trig, sig, env;
    var delay = 0.05;
    var aeq = ~amps;
    var del = ~delays + (0.0, 0.01..0.59);

    // source impulse
    trig = Impulse.ar(0);
    sig = Decay.ar(trig, decay) * 20000 + 20;
    sig = SinOsc.ar(sig);
    sig = DelayN.ar(sig, delay, delay);
    sig = sig * EnvGen.ar(Env([0, 1, 1, 0], [delay, decay, delay]), trig);
    DetectSilence.ar(sig, time: 1, doneAction: Done.freeSelf);
    sig = sig * amp;
    sig = sig * aeq; // amplitude compensation
    sig = DelayN.ar(sig, maxdelaytime: 2, delaytime: del) // delay to synchronize reflections at sweetspot;
    Out.ar(out, sig)
}).add
)

// Impulse (reflections arrival should be simultaneous and of equal volume at sweetspot)
(
SynthDef(\impulse_simul, { |out=0, amp=0.1, decay=0.015|
    var trig, sig, env;
    var delay = 0.05;
    var aeq = ~amps;
    var del = ~delays;

    // source impulse
    trig = Impulse.ar(0);
    sig = Decay.ar(trig, decay) * 20000 + 20;
    sig = SinOsc.ar(sig);
    sig = DelayN.ar(sig, delay, delay);
    sig = sig * EnvGen.ar(Env([0, 1, 1, 0], [delay, decay, delay]), trig);
    DetectSilence.ar(sig, time: 1, doneAction: Done.freeSelf);
    sig = sig * amp;
    // channel delay
    sig = DelayN.ar(sig, maxdelaytime: 1, delaytime: del);
    sig = sig * aeq; // amplitude compensation
    Out.ar(out, sig)
}).add
)

// Play impulse
x = Synth(\impulse_spread);
y = Synth(\impulse_simul);
x.free;
y.free;

// Repeated spread impulses
(
Pdef(\pulse,
    Pbind(
        \instrument, \impulse_spread,
        \dur, 1
    );
).play;
)
Pdef(\pulse).stop;

// Repeated simultaneous impulses
(
Pdef(\pulse,
    Pbind(
        \instrument, \impulse_simul,
        \dur, 1
    );
).play;
)
Pdef(\pulse).stop;
