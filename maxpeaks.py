import pandas as pd
import argparse

parser = argparse.ArgumentParser(
    description='Generate CSV file of main reflexion for each speaker')
parser.add_argument(
    '-n', '--test_number',
    default=1,
    metavar='',
    type=int,
    help='number of test for which to create CSV (not the total number of tests)')
parser.add_argument(
    '-s', '--number_speakers',
    default=1,
    metavar='',
    type=int,
    help='number of speakers in test')


args = parser.parse_args()


def maxpeaks(test_number: int,
             number_speakers: int):

    for i in range(1, 12 * number_speakers + 1):
        csv_file_name = "CSV_goupillon_test" + str(test_number) + "_speaker" + str(i) + ".csv"

        df = pd.read_csv(csv_file_name, sep='\t')
        maxpeak = df['Pressure'].argmax()

        newdf = df[df["#"] == maxpeak]
        if i == 1:
            newdf.to_csv("MAXCSV_goupillon_test" + str(test_number) + ".csv", index=False, mode='a', header=True, sep='\t')
        else:
            newdf.to_csv("MAXCSV_goupillon_test" + str(test_number) + ".csv", index=False, mode='a', header=False, sep='\t')


if __name__ == "__main__":
    maxpeaks(
        args.test_number,
        args.number_speakers)
