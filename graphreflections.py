import argparse
from matplotlib import cm, colors
import matplotlib.pyplot as plt
import numpy as np
import mpl_toolkits.mplot3d as plt3d
import matplotlib.pylab as pl
import pandas as pd

from utils import plotting as P

parser = argparse.ArgumentParser(
    description='Generate graph of main reflexions for goupillon test')
parser.add_argument(
    '-n', '--test_number',
    default=1,
    metavar='',
    type=int,
    help='number of the test to graph')

args = parser.parse_args()


def graphreflexions(test_number: int):

    df = pd.read_csv("MAXCSV_goupillon_test" + str(test_number) + ".csv", sep='\t')

    number = df['#'].to_numpy()
    pressure = df['Pressure'].to_numpy()
    theta = df['Theta'].to_numpy()
    phi = df['Phi'].to_numpy()
    time = df['Time'].to_numpy()

    time_rel = time - np.min(time)

    # Plot data
    # Set figure
    fig = plt.figure()
    fig.set_size_inches(10, 10)
    ax = fig.add_subplot(111, projection='3d')
    ax.set_xlim(-1, 1)
    ax.set_ylim(-1, 1)
    ax.set_zlim(-1, 1)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    ax.view_init(azim=120)

    x, y, z = P.spherical_to_cartesian(pressure/np.max(pressure), theta, phi)

    norm2 = colors.Normalize(
        vmin=0,
        vmax=np.max(time_rel) * 1000)
    cmap = cm.hsv
    fig.colorbar(
        cm.ScalarMappable(
            norm=norm2,
            cmap=cmap),
        ax=ax,
        shrink=0.8,
        label='delay (ms)')
    color_d = pl.cm.hsv(
        norm2(
            time_rel * 1000))

    for i in range(np.size(x)):
        line = plt3d.art3d.Line3D((0, x[i]), (0, y[i]), (0, z[i]), c=color_d[i])
        ax.add_line(line)

    plt.show()


if __name__ == "__main__":
    graphreflexions(
        args.test_number)

