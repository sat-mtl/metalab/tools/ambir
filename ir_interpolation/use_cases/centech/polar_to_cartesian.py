import os
import math

# storing the path to the current directory
directory = os.getcwd()
i = 0

for filename in os.listdir(directory):
    
    if filename[0] == 'I' and filename[3] == 'R':
        
        # getting the rayon and the angle from the filename
        deg = int(filename.split('a')[1].split('_')[0])
        rayon = int(filename.split('P')[1].split('m')[0])

        # creating a new name with cartesian coordinates.
        newname = f"IR_({round(rayon * math.cos(math.radians(deg)), 2)}_{round(rayon * math.sin(math.radians(deg)), 2)}).wav"
        i+=1

        # renaming the file
        os.rename(directory+"/"+filename, directory+"/"+newname)

        print(newname)