import os
import soundfile as sf
import numpy as np

# here, put the relative path of where the IRs to edit are located. 
source_directory = "../../../../../Documents/IR_Centech/day2/"
abs_path_to_dir = os.path.abspath(source_directory)

for filename in os.listdir(abs_path_to_dir):
    
    # reading the file and storing it in an np.ndarray 
    output, samplerate = sf.read(abs_path_to_dir+"/"+filename)
    number_of_channels = int(output[0].size)
    
    # start and end time of the IR to edit (in seconds). 
    start = 0.5
    end = 9
    first_frame = int(samplerate * start)
    last_frame = int(samplerate * end)
    total_length = last_frame - first_frame
    data = np.ndarray((total_length, number_of_channels))

    # copying the frames that we want from the file into the data array 
    for channel in range(number_of_channels):
        data[:, channel] = output[:, channel][first_frame:last_frame]

    # edited IRs will be written in the current working directory. 
    sf.write(filename, data, samplerate)