#!/bin/bash

volume=-1
ntests=-1
delay=-1
speakers=-1


while getopts 'v:n:d:s:' c
do
	case $c in
		v) volume=$OPTARG;;
		n) ntests=$OPTARG;;
		d) delay=$OPTARG;;
		s) speakers=$OPTARG;;
	esac
done

if [[ $volume = -1  ||  $ntests = -1  ||  $delay = -1 ||  $speakers = -1 ]]
then
	echo "Error: please specify dBFS volume (-v), number of tests (-n) and speakers (-s), and delay (-d)"
else	

	sleep $delay

	for j in $(seq 1 $ntests); do

		ecasound -a:1 -f:16,19,48000 -i jack,zylia -f:,19,48000 -o REC_goupillon_test$j\_speaker1.wav -a:2 -f:,60,48000 -i ess.wav -o jack,system -t:22 -eadb:$volume

		zylia-ambisonics-converter --channelOrdering ACN --input REC_goupillon_test$j\_speaker1.wav --output B_goupillon_test$j\_speaker1.wav --device ZM-1-3E

		python3 farina_deconv.py -r B_goupillon_test$j\_speaker1.wav -f filter.npy -i IR_goupillon_test$j\_speaker1.wav

		python3 direction_of_arrival_plot.py -f IR_goupillon_test$j\_speaker1.wav -a -e -s


		for i in $(seq 2 60); do

			ecasound -a:1 -f:16,19,48000 -i jack,zylia -f:,19,48000 -o REC_goupillon_test$j\_speaker$i.wav -a:2 -f:,60,48000 -i ess.wav -chcopy:1,$i  -eac:0,1 -o jack,system -t:22 -eadb:$volume
			
			zylia-ambisonics-converter --channelOrdering ACN --input REC_goupillon_test$j\_speaker$i.wav --output B_goupillon_test$j\_speaker$i.wav --device ZM-1-3E

			python3 farina_deconv.py -r B_goupillon_test$j\_speaker$i.wav -f filter.npy -i IR_goupillon_test$j\_speaker$i.wav
			
			python3 direction_of_arrival_plot.py -f IR_goupillon_test$j\_speaker$i.wav -a -e -s

		done

		python3 maxpeaks.py -n $j -s $speakers

		python3 makespatializer.py

		mkdir test_$j
		mv REC_goupillon_test$j* test_$j
		mv B_goupillon_test$j* test_$j
		mv IR_goupillon_test$j* test_$j
		mv CSV_goupillon_test$j* test_$j
		mv MAXCSV_goupillon_test$j* test_$j
		mv reflectVBAP_test$j* test_$j

	done
fi
