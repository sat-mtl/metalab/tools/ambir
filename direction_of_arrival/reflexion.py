import numpy as np
from typing import List


class Reflexion:

    _speed_of_sound = 343.0
    _fs = 48000

    def __init__(self, signal, arrival_sample):
        self._arrival_sample = arrival_sample
        self._signal = signal
        self._w = signal[:, 0]
        self._x = signal[:, 3]
        self._y = signal[:, 1]
        self._z = signal[:, 2]
        self._Ix = self._w * self._x
        self._Iy = self._w * self._y
        self._Iz = self._w * self._z
        self._modulus_I = np.sqrt(self._Ix**2 + self._Iy**2 + self._Iz**2)
        self._energy_density = (
            self._w**2 + self._x**2 + self._y**2 + self._z**2) / self._speed_of_sound

    def get_intensity_vector(self) -> List[float]:
        Ix_average = np.nanmean(self._Ix)
        Iy_average = np.nanmean(self._Iy)
        Iz_average = np.nanmean(self._Iz)

        return Ix_average, Iy_average, Iz_average

    def get_pressure(self) -> List[float]:
        pressure = np.nanmean(self._w)
        return pressure

    def get_intensity_modulus(self) -> float:
        modulus_I_average = np.nanmean(
            self._modulus_I)

        return modulus_I_average

    def get_doa_mean(self) -> List[float]:
        x, y, z = self.get_intensity_vector()
        theta = np.arctan2(y, x)
        modulus = self.get_intensity_modulus()
        phi = np.arcsin(z / modulus)

        return theta, phi

    def get_diffusivity(self) -> float:
        ratio_d = self._modulus_I / \
            (self._energy_density * self._speed_of_sound)
        ratio_d_average = np.nanmean(ratio_d)
        diffusivity = 1 - ratio_d_average

        return diffusivity

    def get_energy_density(self) -> float:
        energy_density_average = np.nanmean(
            self._energy_density)

        return energy_density_average

    @property
    def arrival_sample(self) -> int:
        return self._arrival_sample
