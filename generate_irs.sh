#!/bin/bash

declare -i num=1
for f in /home/hteyssier/Documents/IR_MaisonSymphonique/Atelier7Hz_ambIRs/iteration3/Bformat/01-up/Zylia/*.wav; do
    echo $f
    name="iteration3_01-up_${num}.wav"
    python3 farina_deconv.py -f filter.npy -r $f -i $name
    num=($num+1)
done 

echo "Done 2"
